DIAdem-Dateikonverter
=====================

Skript zur ordnerweisen Konvertierung von Dateien, die mit NI DIAdem gelesen werden können.

Die möglichen Pfade für die Konvertierung sind in der folgenden Abbildung dargestellt. Grün bedeutet, dass die entsprechende Konvertierung möglich ist; rot kennzeichnet nicht mögliche Pfade (Beispiel: ``bin -> csv`` ist möglich, ``csv -> bin`` ist nicht möglich).

.. image:: ./media/conversion-matrix.png
   :width: 500pt

Voraussetzungen
---------------

* Installation von NI DIAdem
* Für Dateitypen, die NI DIAdem nicht standardmäßig bekannt sind, müssen entsprechende Plugins installiert werden. Diese werden von NI zur Verfügung gestellt:

  * HBM Catman (Dateiendung ``bin``): http://www.ni.com/example/29043/en/
  * Matlab (Dateiendung ``mat``): http://www.ni.com/example/29178/en/
  * IMC Famos (Dateiendung ``dat`` oder ``raw``): https://www.ni.com/de-de/support/downloads/dataplugins/download.imc-dataworks-dataplugin-for-famos.html#37231

Benutzung
---------

* Skript mit DIAdem öffnen
* Die Konfiguration erfolgt über die Variablen in Zeilen 51-55

===================  ===================
Variable             Erklärung
===================  ===================
``sPathSource``      Absoluter Pfad des Verzeichnisses, das die Quelldaten enthält
``sPathTarget``      Absoluter Pfad des Verzeichnisses, in dem die konvertierten Daten abgelegt werden sollen
``sPathSignalList``  Absoluter Pfad der Signalliste\* (optional)
``sFormatSource``    Bezeichnung des Dateiformats für Quelldateien
``sFormatTarget``    Bezeichnung des Dateiformats für Zieldateien
===================  ===================

\* Die Signalliste ist eine Textdatei, die zeilenweise die Namen der Signale enthält, die bei der Konvertierung berücksichtigt werden sollen. Alle anderen Signale werden ignoriert und sind nicht in der Zieldatei enthalten. Zeitkanäle (gekennzeichnet durch die Kanalnamen *Langsame Messrate*, *Standardmessrate* oder *Schnelle Messrate*) sind hiervon ausgenommen. Wenn ``sPathSignalList`` einen Leerstring enthält, werden alle Kanäle der Quelldatei in die Zieldatei übernommen.

* Das Skript wird über den entsprechenden Button ausgeführt. In der Statusleiste werden Informationen über den Fortschritt eingeblendet.


Hinweis
-------

Für Fehlerfreiheit und Vollständigkeit wird keine Gewähr übernommen. Bei Fragen kann michael.pagitsch@cwd.rwth-aachen gerne kontaktiert werden. Merge-Requests sind ebenfalls willkommen.

